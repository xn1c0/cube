#ifndef CUBE_H_
#define CUBE_H_

#include <iostream>
#include <exception>
#include "bad_build_size.hpp"
#include "index_out_of_bounds.hpp"
#include "containers_size_not_match.hpp"

/**
 * @author 844837 Plebani Nicolo'
 * @file cube.hpp
 * @brief Declaration of cube class.
 * This file contains the templated class cube.
 */

/**
 * @brief Class cube.
 * This class implements a 3D-Matrix
 * Equality between two type T value is verified using a functor of equality E.
 * The values stored in this container are of generic type T.
 * @see cube_default_equality struct
 * @tparam T type of cube elements value
 * @tparam E functor that establishes the equality policy between two cube elements
 */
template<class T, typename E>
class cube {
    public:
        typedef T value_type;
        typedef E equality_type;
        typedef long int index_type;
        typedef unsigned int size_type;
        typedef value_type *iterator;
        typedef const value_type *const_iterator;
    /**
     * @brief Default onstructor.
     * Default constructor to instantiate a type cube object.
     * @post _cube == nullptr
     * @post _z == _y == _x == 0
     */
    cube(): _cube(nullptr), _size(0), _z(0), _y(0), _x(0) {}
    /**
     * @brief Secondary constructor.
     * This constructor provides the functionality to instantiate a type cube
     * object with provided number of z pages, y rows and x columns. All the
     * values contained by the cube are not initialized.
     * @param z number of pages
     * @param y number of rows
     * @param x number of columns
     * @pre (z > 0 && y > 0 && x > 0)
     * @post cube != nullptr
     * @post _size == _z * _y * _x
     * @post _z == z
     * @post _y == y
     * @post _x == x
     * @throw bad_build_size custom exception throwed if pages, row, column size is <= 0
     * @throw std::bad_alloc possible allocation error coming from new[]
    */
    cube(const index_type z, const index_type y, const index_type x): _cube(nullptr), _size(0), _z(0), _y(0), _x(0) {
      if (z <= 0 || y <= 0 || x <= 0)
        throw bad_build_size("Impossible to create a cube of negative or zero size.");
      size_type size = z * y * x;
      _cube = new value_type[size];
      _size = size;
      _z = z; _y = y; _x = x;
    }
    /**
     * @brief Secondary constructor
     * This constructor provides the functionality to instantiate a type cube
     * object with provided number of z pages, y rows and x columns. It is
     * also provided an initial value to fill the cube with.
     * @param z number of pages
     * @param y number of rows
     * @param x number of columns
     * @param init the initial value to fill the cube with
     * @pre (z > 0 && y > 0 && x > 0)
     * @post cube != nullptr
     * @post _size == _z * _y * _x
     * @post _z == z
     * @post _y == y
     * @post _x == x
     * @throw bad_build_size custom exception throwed if pages, row, column size is <= 0
     * @throw std::exception possible exception coming from generic type T
     * @throw std::bad_alloc possible allocation error coming from new[]
    */
    cube(const index_type z, const index_type y, const index_type x, const value_type &init): _cube(nullptr), _size(0), _z(0), _y(0), _x(0) {
      if (z <= 0 || y <= 0 || x <= 0)
        throw bad_build_size("Impossible to create a cube of negative or zero size.");
      size_type size = z * y * x;
      _cube = new value_type[size];
      _size = size;
      _z = z; _y = y; _x = x;
      try {
        for(size_type i = 0; i < size; ++i)
          _cube[i] = init;
      } catch(...) {
        clear();
        throw;
      }
    }
    /**
     * @brief Secondary constructor
     * This constructor allows an object of type cube with T values to be
     * instantiated from a second cube object of Q values.
     * The cube object of T values will be filled with the contents of the
     * cube object of Q values.
     * There is a type conversion from Q values to T values.
     * @param other source cube<Q>
     * @post _cube != nullptr
     * @post _size == other.size
     * @post _z == other.page_size()
     * @post _y == other.row_size()
     * @post _x == other.column_size()
     * @throw std::bad_alloc possible allocation error coming from new[]
     * @throw std::bad_cast possible cast error from fill method
     * @tparam Q other cube type of elements
     * @tparam E other cube equality functor
     */
    template<typename Q, typename S>
    cube(const cube<Q, S> &other): _cube(nullptr), _size (0), _z(0), _y(0), _x(0) {
      size_type size = other.size();
      _cube = new value_type[size];
      _size = size;
      _z = other.page_size(); _y = other.row_size(); _x = other.column_size();
      try {
        fill(other.begin(), other.end());
      } catch(...) {
        clear();
        throw;
      }
    }
    /**
     *  @brief Destructor
     *  Simply call the clear method to deallocate resources.
     *  This grants RAII pattern.
     */
    virtual ~cube() { clear(); }
    /**
     * @brief Copy constructor.
     * @param other source cube to copy
     * @post cube != nullptr
     * @post _size == other._size
     * @post _z == other._z
     * @post _y == other._y
     * @post _x == other._x
     * @throw std::bad_alloc possible allocation error coming from new[]
     */
    cube(const cube &other): _cube(nullptr), _size (0), _z(0), _y(0), _x(0) {
      _cube = new value_type[other._size];
      _size = other._size;
      _z = other._z;
      _y = other._y;
      _x = other._x;
      try {
        for (size_type i = 0; i < other._size; ++i)
          _cube[i] = other._cube[i];
      } catch (...) {
        clear();
        throw;
      }
    }
    /**
     * @brief Operator=
     * @param other source cube to copy
     * @return reference to this
     * @post cube != nullptr
     * @post _size == other._size
     * @post _z == other._z
     * @post _y == other._y
     * @post _x == other._x
     * @throw std::bad_alloc possible allocation error coming from new[]
     */
    cube &operator=(const cube &other) {
      if (this != &other) {
        cube tmp(other);
        this->swap(tmp);
      }
      return *this;
    }
    /**
     * @brief Operator==
     * Test if every value contained by two cube is the same.
     * It is used the _equality functor instance to check if two elements
     * are equal. At first check if sizes match then check if every value
     * stored by the cubes matches.
     * @param other cube to compare to this
     * @return true if cubes are equals else false
     */
    bool operator==(const cube &other) const {
      if (_size != other._size)
        return false;
      size_type i = 0;
      while(i < _size) {
        if(!_equality(_cube[i], other._cube[i]))
          return false;
        ++i;
      }
      return true;
    }
    /**
     * @brief Operator!=
     * @param other cube to compare to this
     * @return true if cubes are not equals else false
     */
    bool operator!=(const cube &other) const { return !(*this == other); }
    /**
     * @brief cube class swap method.
     * Swaps data stored by two cubes.
     * @param other cube to swap data with
     */
    void swap(cube &other) {
      std::swap(_cube, other._cube);
      std::swap(_size, other._size);
      std::swap(_z, other._z);
      std::swap(_y, other._y);
      std::swap(_x, other._x);
    }
    /**
     * @brief Getter/Setter of location indexed by (page, row, column)
     * Provides functionality to retrieve/modify a value in this cube indexed
     * by (page, size, column) a.k.a. (_z, _y, _x) (operator style).
     * @param z page index
     * @param y row index
     * @param x column index
     * @return value reference to location indexed by (z, y, x)
     * @pre z >= 0 && y >= 0 && x >= 0
     * @pre z < _z && y < _y && x < _x
     * @throw index_out_of_bounds custom exception throwed if any of given indexes exceed cube bounds
     */
    value_type &operator()(const index_type z, const index_type y, const index_type x) {
      if (z < 0 || y < 0 || x < 0 || _z <= z || _y <= y || _x <= x)
        throw index_out_of_bounds("Provided indexes exceed cube bounds");
      return _cube[index(z, y, x)];
    }
    /**
     * @brief Getter of location indexed by (page, row, column)
     * Provides functionality to retrieve/modify a value in this cube indexed
     * by (page, size, column) a.k.a. (_z, _y, _x) (operator style).
     * @param z page index
     * @param y row index
     * @param x column index
     * @return value reference to location indexed by (z, y, x)
     * @pre z >= 0 && y >= 0 && x >= 0
     * @pre z < _z && y < _y && x < _x
     * @throw index_out_of_bounds custom exception throwed if any of given indexes exceed cube bounds
     */
    const value_type &operator()(
      const index_type z,
      const index_type y,
      const index_type x)
    const {
      if (z < 0 || y < 0 || x < 0 || _z <= z || _y <= y || _x <= x)
        throw index_out_of_bounds("Provided indexes exceed cube bounds");
      return _cube[index(z, y, x)];
    }
    /**
     * @brief Getter/Setter of location indexed by (page, row, column)
     * Provides functionality to retrieve/modify a value in this cube indexed
     * by (page, size, column) a.k.a. (_z, _y, _x) (C++ style).
     * @param z page index
     * @param y row index
     * @param x column index
     * @return value reference to location indexed by (z, y, x)
     * @pre z >= 0 && y >= 0 && x >= 0
     * @pre z < _z && y < _y && x < _x
     * @throw index_out_of_bounds custom exception throwed if any of given indexes exceed cube bounds
     */
    value_type &value(const index_type z, const index_type y, const index_type x) {
      if (z < 0 || y < 0 || x < 0 || _z <= z || _y <= y || _x <= x)
        throw index_out_of_bounds("Provided indexes exceed cube bounds");
      return _cube[index(z, y, x)];
    }
    /**
     * @brief Getter of location indexed by (page, row, column)
     * Provides functionality to retrieve/modify a value in this cube indexed
     * by (page, size, column) a.k.a. (_z, _y, _x) (C++ style).
     * @param z page index
     * @param y row index
     * @param x column index
     * @return value reference to location indexed by (z, y, x)
     * @pre z >= 0 && y >= 0 && x >= 0
     * @pre z < _z && y < _y && x < _x
     * @throw index_out_of_bounds custom exception throwed if any of given indexes exceed cube bounds
     */
    const value_type &value(
      const index_type z,
      const index_type y,
      const index_type x
    ) const {
      if (z < 0 || y < 0 || x < 0 || _z <= z || _y <= y || _x <= x)
        throw index_out_of_bounds("Provided indexes exceed cube bounds");
      return _cube[index(z, y, x)];
    }
    /**
     * @brief Getter of _size
     * @return _size
     */
    size_type size() const { return _size; }
    /**
     * @brief Getter of _z number of pages
     * @return _z
     */
    size_type page_size() const { return _z; }
    /**
     * @brief Getter of _y number of rows per page
     * @return _y
     */
    size_type row_size() const { return _y; }
    /**
     * @brief Getter of _x number of columns per page
     * @return _x
     */
    size_type column_size() const { return _x; }
    /**
     * @brief Creates a new sub-cube from this
     * Creates a new sub-cube by copy from this with the provided indexes.
     * The size of the new sub-cube will be (z2 - z1) * (y2 - y1) * (x2 - x1).
     * The elements in the new sub-cube will go from z1 page to z2 page, from y1
     * row to y2 row and from x1 column to x2 column.
     * The new sub-cube sizes will be the followings:
     * pages = z2 - z1 + 1, rows = y2 - y1 + 1 and columns = x2 - x1 + 1
     * Since the cube is logically stored in _cube dynamic array, initialize the
     * subptr to sub._cube, iterate on every page, every row and every column.
     * On every column access increment subptr to point to sub._cube next column.
     * Since the access il logical there are implied a lot of multiplications.
     * Without optimizations there are needed:
     * (z2 - z1 + 1) * (y2 - y1 + 1) * (x2 - x1 + 1)
     * To optimize this method, store relative offset on every iteration.
     * Now the multiplications are reduced to (z2 - z1 + 1) * (y2 - y1 + 1)
     * @param z1 from page index
     * @param z2 to page index
     * @param y1 from row index
     * @param y2 to row index
     * @param x1 from column index
     * @param x2 to column index
     * @return sub by value, the new created sub-cube
     * @pre z1 <= z2 && y1 <= y2 && x1 <= x2
     * @pre z1 >= 0 && y1 >= 0 && x1 >= 0
     * @pre z2 < _z && y2 < _y && x2 < _x
     * @throw index_out_of_bounds custom exception throwed if any of given indexes exceed cube bounds
     * @throw bad_build_size custom exception throwed from constructor if (z2 < z1), (y2 < y1) or (x2 < x1)
     */
    cube slice(
        const index_type z1, const index_type z2,
        const index_type y1, const index_type y2,
        const index_type x1, const index_type x2
    ) const {
      if (z1 < 0 || y1 < 0 || x1 < 0 || _z <= z2 || _y <= y2 || _x <= x2)
        throw index_out_of_bounds("Provided indexes exceed cube bounds");
      // if index1 > index2 throw bad_build_size
      cube sub(z2 - z1 + 1, y2 - y1 + 1, x2 - x1 + 1);
      value_type *subptr = sub._cube;
      for(size_type i = z1; i <= z2; ++i) {
        size_type z_offset = i * _y * _x; // page * row_size * column_size
        for(size_type j = y1; j <= y2; ++j) {
          size_type y_offset = j * _x; // row * column_size
          for(size_type k = x1; k <= x2; ++k, ++subptr)
            *subptr = _cube[z_offset + y_offset + k]; // column
        }
      }
      return sub;
    }
    /**
     * @brief Fills cube with generic iterators
     * Fills the cube with other collection generic iteterators.
     * Since can be passed any kind of iterator, its type is not known.
     * In order to check if the 2 containers size match, the only way is to
     * make a tmp cube and try to fill it. During the iteration check if any
     * of the two iterators has reached the end than if any of the two iterators
     * has not reached the end then throw containers_size_not_match.
     * Else store tmp cube in *this.
     * @param begin iterator of a generic collection
     * @param end iterator of a generic collection
     * @pre _size == number of elements from begin to end
     * @post _cube dynamic array contains same values of the iterated collection
     * @throw containers_size_not_match custom exception throwed if containers size not match
     * @throw std::bad_cast possible cast exception
     * @tparam I generic iterator of a collection
     */
    template<typename I>
    void fill(I begin, I end) {
      cube tmp(*this);
      iterator it = tmp.begin(), en = tmp.end();
      while(it != en && begin != end)
        *(it++) = static_cast<value_type>(*(begin++));
      if (it != en)
        throw containers_size_not_match("Cube size is smaller than the other container");
      if(begin != end)
        throw containers_size_not_match("Cube size is bigger than the other container");
      *this = tmp;
    }
    /**
     * @brief Redefining ostream operator
     * Redefing  ostream operator to make cube printable on the output buffer.
     * This is used to understand where and how data is stored in the collection
     * @param os output stream (lhs)
     * @param c cube to print (rhs)
     * @return reference to output ostream
     */
    friend std::ostream& operator<<(std::ostream &os, const cube<value_type, equality_type> &c) {
      for(size_type i = 0; i < c._z; ++i) {
        size_type z_offset = i * c._y * c._x; // page * row_size * column_size
        os << "Page <" << i << '>' << std::endl;
        for(size_type j = 0; j < c._y; ++j) {
          size_type y_offset = j * c._x; // row * column_size
          for(size_type k = 0; k < c._x; ++k)
            os << c._cube[z_offset + y_offset + k] << ' ';
          os << std::endl;
        }
        os << std::endl;
      }
      return os;
    }
    /**
     * @brief Iterator to the first element of the collection
     * @return iterator to the first element of the collection
     */
    iterator begin() { return _cube; }
    /**
     * @brief Iterator to the last element of the collection
     * @return iterator to the last element of the collection
     */
    iterator end() { return _cube + _size; }
    /**
     * @brief Constant iterator to the first element of the collection
     * @return const_iterator to the first element of the collection
     */
    const_iterator begin() const { return _cube; }
    /**
     * @brief Constant iterator to the last element of the collection
     * @return const_iterator to the last element of the collection
     */
    const_iterator end() const { return _cube + _size; }

    private:
        equality_type _equality; ///< equality functor instance
        value_type *_cube; ///< pointer to the dynamic array representing the logical cube
        size_type _size; ///< size of the dynamic array _cube
        size_type _z; ///< numbers of pages
        size_type _y; ///< numbers of rows per page
        size_type _x; ///< number of columns per page
        /**
         * @brief Clear all allocated memory
         * Deallocate all memory previously allocated by constructors.
         * This is to ensure the RAII pattern is respected.
         * @post _cube = nullptr
         * @post _z = _y = _x = 0
         */
        void clear() {
          delete[] _cube;
          _cube = nullptr;
          _size = _z = _y = _x = 0;
        }
        /**
         * @brief Calculate the location indexed by (z, y, x)
         * This is a core method, it provides the policy to access the array.
         * At the moment it is used the page-wise and row-wise access, meaning
         * first page...last page, first row...last row and first column...last
         * column. A little scheme to help {pages} [rows] (columns) below.
         * p1{ r1[ c1() c2() c3() c4()] r2[c1() c2() c3() c3()] }  p2{ ... } ...
         * As you can see there is a logical structure to access the dynamic array
         * @return index to access location
         */
        inline size_type index(size_type z, size_type y, size_type x) const {
          /**
           * page-wise and row-wise access to the cube.
           * page * row_size * column_size + row * column_size + column
           */
          return z * _y * _x + y * _x + x;
        }
};

/**
 * Global function that recives a t cube of type T and a f Functor of type F.
 * Returns a new cube q of type Q of which elements are obtained by applying f
 * on c elements like so -> b(k,i,j) = f(c(k,i,j)).
 * @param t source cube
 * @param f functor that manipulates data
 * @return q by copy, the new cube
 * @throws std::bad_cast possible cast exception
 * @tparam Q new cube elements value type
 * @tparam S new cube equality functor
 * @tparam T current cube elements value type
 * @tparam E current cube equality functor
 * @tparam F functor to convert T to Q
*/
template<typename Q, typename S, typename T, typename E, typename F>
cube<Q, S> transform(const cube<T, E> &t, const F f) {
  cube<Q, E> q(t.page_size(), t.row_size(), t.column_size());
  typename cube<T, E>::const_iterator itt = t.begin();
  typename cube<Q, S>::iterator itq = q.begin();
  while(itt != t.end())
    *(itq++) = f(*(itt++));
  return q;
}

#endif // CUBE_H_
