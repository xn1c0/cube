#include <iostream>
#include <cassert>
#include <vector>
#include <list>
#include <iterator>
#include <algorithm>
#include "cube.hpp"

using namespace std;

/**
 * @brief Struct used to test cube
 */
struct person {
  std::string _first_name;
  std::string _last_name;

  person(): _first_name("luigi"), _last_name("rossi") {}

  person(const std::string& first_name,
         const std::string& last_name) :
    _first_name(first_name),
    _last_name(last_name) {}

  operator int() const {
    return _first_name.size() + _last_name.size();
  }

  friend std::ostream& operator<<(std::ostream &os, const person &c) {
    os << c._first_name << " " << c._last_name;
    return os;
  }
};

/**
 * @brief Functor to check equality between two int
 */
struct int_eq {
  bool operator()(const int x, const int y) const {
    return x == y;
  }
};


/**
 * @brief Functor to check equality between two doubles
 */
struct double_eq {
  bool operator()(const double x, const double y) const {
    return x == y;
  }
};


/**
 * @brief Functor to check equality between two int vectors
 */
struct intv_eq {
  bool operator()(const vector<int> &x, const vector<int> &y) const {
    return x == y;
  }
};

/**
 * @brief Functor to check equality between two doubles vectors
 */
struct doublev_eq {
  bool operator()(const vector<double> &x, const vector<double> &y) const {
    return x == y;
  }
};

/**
 * @brief Functor to check equality between two char pointers
 */
struct charp_eq {
  bool operator()(const char *x, const char *y) const {
    while(*x != '\0' && *y != '\0')
      if(*(x++) != *(y++))
         return false;
    return true;
  }
};

/**
 * @brief Functor to check equality between two std::string
 */
struct string_eq {
  bool operator()(const string &x, const string &y) const {
    return x == y;
  }
};

/**
 * @brief Functor to check equality between two cube<double, double_eq> cubes
 */
struct doublec_eq {
  bool operator()(const cube<double, double_eq> &x, const cube<double, double_eq> &y) const {
    return x == y;
  }
};

/**
 * @brief Functor to check equality on two person
 */
struct person_eq {
  bool operator()(const person &p1, const person &p2) const {
    return p1._first_name == p2._first_name;
  }
};

/**
 * @brief Functor to check if a int number x == y mod3
 */
struct z3_eq {
  bool operator()(const int x, const int y) const {
    return x % 3 == y % 3;
  }
};

/**
 * @brief Functor to convert double to int
 */
struct double_to_int {
  int operator()(const double x) const {
    return static_cast<int>(x);
  }
};

/**
 * @brief Functor to convert char* to int
 * Sum of ASCII char code
 */
struct charp_to_int {
  int operator()(const char *x) const {
    int r = 0;
    for(; *x != '\0'; ++x)
      r += *x;
    return r;
  }
};


/**
 * @brief Functor to convert vector<double> to vector<int>
 */
struct vectordouble_to_int {
  vector<int> operator()(const vector<double> &x) const {
    vector<int> r;
    for(vector<double>::size_type i = 0; i < x.size(); i++)
      r.push_back(x[i]);
    return r;
  }
};


/**
 * @brief Functor to convert cube<double, double_eq> to vector<int>
 */
struct cubedouble_to_vectorint {
  template<typename E>
  vector<int> operator()(const cube<double, E> &x) const {
    vector<int> r(x.size(), 0);
    vector<int>::iterator itr = r.begin();
    typename cube<double, E>::const_iterator itx = x.begin();
    for(; itr != r.end(); ++itr, ++itx)
      *itr = static_cast<int>(*itx);
    return r;
  }
};

/**
 * @brief functor to convert int into a person
 */
struct int_to_person {
  person operator()(const int x) const {
    if(x < 10)
      return person("luca", "verdi");
    else
      return person("francesco", "neri");
  }
};

/**
 * @brief Print std::vector
 */
template <typename S>
ostream& operator<<(ostream& os, const vector<S>& vector) {
  for (auto element : vector)
    os << element << " ";
  return os;
}

/**
 * @brief Function to check if sizes of a cube are correct
 * @param c the cube to test
 * @param x the number of pages c must have
 * @param y the number of rows c must have
 * @param z the number of columns c must have
 * @return true if sizes are correct else false
 */
template<typename Q, typename S>
bool is_sizes_correct(cube<Q, S> &c,
                      typename cube<Q, S>::size_type z,
                      typename cube<Q, S>::size_type y,
                      typename cube<Q, S>::size_type x
) {
  typename cube<Q, S>::size_type elements = z * y * x;
  return c.size() == elements && c.page_size() == z && c.row_size() == y && c.column_size() == x;
}

/**
   @brief Test constructors
*/
void test_ctor() {
  cube<int, int_eq> c1;
  assert(is_sizes_correct(c1, 0, 0, 0));
  cube<int, int_eq> c2(3, 4, 5);
  assert(is_sizes_correct(c2, 3, 4, 5));
  cube<int, int_eq> c3(1, 3, 2, 12);
  assert(is_sizes_correct(c3, 1, 3, 2));
  cube<double, double_eq> c4(c3);
  assert(is_sizes_correct(c4, 1, 3, 2));
}

/**
   @brief Test copy constructors
*/
void test_cctor() {
  cube<int, int_eq> c1(2, 2, 2, 7);
  cube<int, int_eq> c2(c1);
  assert(c1 == c2);
  c1.value(0, 0, 0) = 0;
  assert(c1 != c2);
  const cube<int, int_eq> c3(3, 3, 3, 9);
  cube<int, int_eq> c4 = c3;
  const cube<int, int_eq> c5 = c4;
  assert(c3 == c4);
  assert(c4 == c5);
}

/**
   @brief Test operator=
*/
void test_opeq() {
  cube<int, int_eq> c1(2, 2, 2, 7);
  cube<int, int_eq> c2;
  cube<int, int_eq> c3;
  c3 = c2 = c1;
  assert(c1 == c2);
  assert(c2 == c3);
  c1.value(0, 0, 0) = 0;
  assert(c1 != c2);
  assert(c1 != c3);
  assert(c2 == c3);
  const cube<int, int_eq> c4(2, 2, 3, 10);
  c2 = c4;
  assert(c2 == c4);
  assert(c2 != c3);
}

/**
   @brief Test equality operators
*/
void test_equality() {
  vector<int> v1 = {7, 7, 7};
  vector<int> v2 = {6, 6, 6};
  cube<vector<int>, intv_eq> c1(2, 2, 2, v1);
  cube<vector<int>, intv_eq> c2(2, 2, 2, v2);
  cube<vector<int>, intv_eq> c3(1, 5, 1, v2);
  cube<vector<int>, intv_eq> c4(c2);
  assert(c1 != c2);
  assert(c1 != c3);
  assert(c2 != c3);
  assert(c2 == c4);
  c3 = c1;
  assert(c1 == c3);
  c1 = c2 = c3 = c4;
  assert(c1 == c2);
  assert(c1 == c3);
  assert(c3 == c4);
}

/**
   @brief Test swap
*/
void test_swap() {
  vector<int> v1 = {7, 7};
  vector<int> v2 = {5, 5, 5};
  cube<vector<int>, intv_eq> c1(2, 2, 2, v1);
  cube<vector<int>, intv_eq> c2(3, 3, 3, v2);
  assert(is_sizes_correct(c1, 2, 2, 2));
  assert(is_sizes_correct(c2, 3, 3, 3));
  assert(c1.value(0, 0, 0).size() == 2);
  assert(c2.value(0, 0, 0).size() == 3);
  assert(c1.value(0, 0, 0)[0] == 7);
  assert(c2.value(0, 0, 0)[0] == 5);
  c1.swap(c2);
  assert(is_sizes_correct(c2, 2, 2, 2));
  assert(is_sizes_correct(c1, 3, 3, 3));
  assert(c1.value(0, 0, 0).size() == 3);
  assert(c2.value(0, 0, 0).size() == 2);
  assert(c1.value(0, 0, 0)[0] == 5);
  assert(c2.value(0, 0, 0)[0] == 7);
}

/**
   @brief Test slice
*/
void test_slice() {
  cube<int, int_eq> c1(5, 5, 5);
  vector<int> v(125);
  int c = 0;
  for(auto it = v.begin(); it != v.end(); ++it, ++c)
    *it = c;
  c1.fill(v.begin(), v.end());
  //std::cout << c1;
  cube<int, int_eq> c2 = c1.slice(2, 3, 2, 4, 1, 2);
  //std::cout << c2;
  assert(c2.value(0, 0, 0) == 61);
  assert(c2.value(0, 2, 1) == 72);
  assert(c2.value(1, 1, 1) == 92);
  assert(c2.value(1, 0, 1) == 87);

  cube<int, int_eq> c3(2, 7, 9);
  vector<int> v1(2* 7 * 9);
  int d = 0;
  for(auto it = v1.begin(); it != v1.end(); ++it, ++d)
    *it = d;
  c3.fill(v1.begin(), v1.end());
  // std::cout << c3;
  cube<int, int_eq> c4 = c3.slice(0, 1, 0, 0, 1, 8);
  // std::cout << c4;
  assert(c3.value(1, 0, 7) == c4.value(1, 0, 6));
  assert(c3.value(0, 0, 2) == c4.value(0, 0, 1));
}

/**
   @brief Test fill
*/
void test_fill() {
  cube<int, int_eq> c1(3, 3, 3);
  vector<int> v(27);
  int c = 0;
  for(auto it = v.begin(); it != v.end(); ++it, ++c)
    *it = c;
  //std::cout << c1;
  c1.fill(v.begin(), v.end());
  //std::cout << c1;
  assert(c1.value(0, 0, 0) == 0);
  assert(c1.value(1, 0, 0) == 9);
  assert(c1.value(0, 2, 2) == 8);
  assert(c1.value(1, 2, 2) == 17);
  assert(c1.value(2, 0, 0) == 18);
  assert(c1.value(2, 2, 2) == 26);
}

/**
   @brief Test transform
*/
void test_transform() {

  cube<double, double_eq> cd(2, 2, 2, 9.5);
  double_to_int dti;
  cube<int, int_eq> converted1 = transform<int, int_eq>(cd, dti);
  assert(converted1(0, 0, 0) == 9);
  assert(converted1(0, 0, 0) == 9);
  assert(converted1(1, 1, 1) == 9);

  const char *str = "ddddd";
  cube<const char*, charp_eq> ccp(3, 3, 3, str);
  charp_to_int cpti;
  cube<int, int_eq> converted2 = transform<int, int_eq>(ccp, cpti);
  assert(converted2(0, 0, 0) == 500);
  assert(converted2(0, 0, 0) == 500);
  assert(converted2(1, 1, 1) == 500);

  vector<double> vd(27, 5.5);
  cube<vector<double>, doublev_eq> cvd(3, 3, 3, vd);
  vectordouble_to_int vdti;
  cube<vector<int>, intv_eq> converted3 = transform<vector<int>, intv_eq>(cvd, vdti);
  assert(converted3.value(0, 0, 0)[0] == 5);
  assert(converted3.value(0, 0, 0)[15] == 5);
  assert(converted3.value(2, 2, 2)[26] == 5);

  cube<cube<double, double_eq>, doublec_eq> ccd(2, 2, 2, cd);
  cubedouble_to_vectorint cdtvi;
  cube<vector<int>, intv_eq> converted4 = transform<vector<int>, intv_eq>(ccd, cdtvi);
  assert(converted4(0, 0, 0)[0] == 9);
  assert(converted4(0, 0, 0)[7] == 9);
  assert(converted4(1, 1, 1)[0] == 9);

  cube<int, int_eq> i(2, 5, 7, 0);
  i.value(0, 0, 0) = 15;
  i.value(0, 1, 6) = 15;
  i.value(1, 4, 6) = 15;
  int_to_person itp;
  cube<person, person_eq> p = transform<person, person_eq>(i, itp);
  assert(p.value(1, 0, 1)._first_name == "luca" && p.value(1, 0, 1)._last_name == "verdi");
  assert(p.value(0, 2, 0)._first_name == "luca" && p.value(0, 2, 0)._last_name == "verdi");
  assert(p.value(0, 0, 0)._first_name == "francesco" && p.value(0, 0, 0)._last_name == "neri");
  assert(p.value(0, 1, 6)._first_name == "francesco" && p.value(0, 1, 6)._last_name == "neri");
  assert(p.value(1, 4, 6)._first_name == "francesco" && p.value(1, 4, 6)._last_name == "neri");
  // std::cout << p;
}

/**
 * @brief Test iterators
 */
void test_iterators() {
  cube<double, double_eq> cd(2, 2, 2, 9.5);
  cube<double, double_eq>::iterator it, en, x, y;
  it = cd.begin();
  en = cd.end();
  assert((en - it) == cd.size());
  cd(0, 0, 1) = 25.0;
  cd(1, 1, 1) = 37.0;
  x = cd.begin() + 2;
  x--;
  assert(*x == *(it + 1));
  assert(*x == cd.value(0, 0, 1));
  y = cd.end() - 2;
  y++;
  assert(*y == *(en - 1));
  assert(*y == cd.value(1, 1, 1));

  assert(std::find(cd.begin(), cd.end(), 37.0) != cd.end());
  assert(std::find(cd.begin(), cd.end(), 99.0) == cd.end());
  assert(std::count(cd.begin(), cd.end(), 9.5) == 6);

  string s = "Mariolino";
  cube<string, string_eq> cs(3, 3, 3, s);
  cube<string, string_eq>::const_iterator a, b;
  a = cs.begin();
  b = cs.end();
  assert(a[26] == s);
  assert(a[26] == *(--b));
  cube<string, string_eq>::size_type count = std::count(cs.begin(), cs.end(), s);
  assert(count == cs.size());
}

/**
 * @brief Test custom equality functor
 */
void test_functor_eq1() {
  cube<person, person_eq> cp1(2, 2, 2);
  cube<person, person_eq> cp2(2, 2, 2);
  cube<person, person_eq> cp3(2, 2, 2);
  cp2.value(1, 1, 1) = person("luigi", "verdi");
  cp3.value(1, 1, 1) = person("mario", "rossi");
  assert(cp1 == cp2);
  assert(cp1 != cp3);

  cube<int, z3_eq> ci1(3, 3, 3, 1);
  cube<int, z3_eq> ci2(3, 3, 3, 76);
  cube<int, z3_eq> ci3(3, 3, 3, 75);
  cube<int, int_eq> ci4(ci2);
  assert(ci1 == ci2);
  assert(ci2 == ci1);
  ci1(0, 0, 0) = 2;
  assert(ci2 != ci1);
  assert(ci1 != ci3);
  assert(ci4 == ci2);
  assert(ci2 == ci4);

  cube<int, z3_eq> a(3, 3, 3, 1);
  cube<int, int_eq> b(3, 3, 3, 19);
  assert(a == b);
  assert(b != a);

  const char *str1 = "spiaggia";
  const char *str2 = "mare";
  cube<const char*, charp_eq> cc1(2, 1, 1, str1);
  cube<const char*, charp_eq> cc2(2, 1, 1, str1);
  assert(cc1 == cc2);
  cc2.value(1, 0, 0) = str2;
  assert(cc1 != cc2);

  vector<double> vd1(27, 5.5);
  vector<double> vd2(27, 5.5);
  cube<vector<double>, doublev_eq> cvd1(3, 3, 3, vd1);
  cube<vector<double>, doublev_eq> cvd2(3, 3, 3, vd2);
  assert(cvd1 == cvd2);
  vd2[24] = 7.6;
  cvd2(2, 2, 1) = vd2;
  assert(cvd1 != cvd2);

  cube<double, double_eq> cd1(3, 6, 2, 23.3);
  cube<double, double_eq> cd2(3, 6, 2, 23.3);
  cube<cube<double, double_eq>, doublec_eq> ccd1(2, 1, 15, cd1);
  cube<cube<double, double_eq>, doublec_eq> ccd2(2, 1, 15, cd2);
  assert(ccd1 == ccd2);
  ccd2(1, 0, 12)(2, 5, 0) = 99.9;
  assert(ccd1 != ccd2);
}

/**
 * @brief Test conversion from Q to T
 */
void test_conversion(cube<person, person_eq> &p) {
  cube<int, int_eq> i1(p);
  cube<int, int_eq> i2(7, 2, 5, 10);
  assert(i1 == i2);
  //std::cout << i1;
}

int main(int argc, char *argv[]) {

  test_ctor();
  test_cctor();
  test_opeq();
  test_swap();
  test_fill();
  test_slice();
  test_transform();
  test_iterators();
  test_functor_eq1();
  cube<person, person_eq> tmp(7, 2, 5);
  test_conversion(tmp);

  return 0;
}
