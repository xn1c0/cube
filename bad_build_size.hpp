#ifndef BAD_BUILD_SIZE_H_
#define BAD_BUILD_SIZE_H_

#include <stdexcept>
#include <string>

/**
 * @author 844837 Plebani Nicolo'
 * @file bad_build_size.hpp
 * @brief bad_build_size declaration
 * This file contains the declaration of class bad_build_size, the custom
 * exception throwed if you attempt to create a Cube with pages, row
 * or column size negative or equal to zero.
 */

/**
 * @brief bad_build_size custom exception inherit from  std::invalid_argument
 * This class is derived from std::logic_error and not from std::runtime_error because
 * std::logic_error reports errors that are a consequence of faulty logic within the program
 * like violating logical preconditions and may be preventable.
 */
class bad_build_size: public std::invalid_argument {
    public:
        /**
         * @brief Constructor, accepts a std::string
         * @param message, the error to be displayed (std::string type)
         */
        bad_build_size(const std::string &message): std::invalid_argument(message) {}
};

#endif // BAD_BUILD_SIZE_H_
