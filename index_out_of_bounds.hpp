#ifndef INDEX_OUT_OF_BOUNDS_H_
#define INDEX_OUT_OF_BOUNDS_H_

#include <stdexcept>
#include <string>

/**
 * @author 844837 Plebani Nicolo'
 * @file index_out_of_bounds.hpp
 * @brief index_out_of_bounds declaration
 * This file contains the declaration of class index_out_of_bounds, the custom
 * exception throwed if you attempt to access a nonexistent location in Matrice3D
 */

/**
 * @brief index_out_of_bound custom exception inherit from  std::out_of_range
 * This class is derived from std::logic_error and not from std::runtime_error because
 * std::logic_error reports errors that are a consequence of faulty logic within the program
 * like violating logical preconditions and may be preventable.
 */
class index_out_of_bounds: public std::out_of_range {
    public:
        /**
         * @brief Constructor, accepts a std::string
         * @param message, the error to be displayed (std::string type)
         */
        index_out_of_bounds(const std::string &message): std::out_of_range(message) {}
};

#endif // INDEX_OUT_OF_BOUNDS_H_
