#ifndef CONTAINERS_SIZE_NOT_MATCH_H_
#define CONTAINERS_SIZE_NOT_MATCH_H_

#include <stdexcept>
#include <string>

/**
 * @author 844837 Plebani Nicolo'
 * @file containers_size_not_match.hpp
 * @brief containers_size_not_match declaration
 * This file contains the declaration of class containers_size_not_match, the custom
 * exception throwed if you attempt to fiil a cube with another container iterators and
 * the two container sizes are not equal.
 */

/**
 * @brief containers_size_not_match custom exception inherit from  std::invalid_argument
 * This class is derived from std::logic_error and not from std::runtime_error because
 * std::logic_error reports errors that are a consequence of faulty logic within the program
 * like violating logical preconditions and may be preventable.
 */
class containers_size_not_match: public std::invalid_argument {
    public:
        /**
         * @brief Constructor, accepts a std::string
         * @param message, the error to be displayed (std::string type)
         */
        containers_size_not_match(const std::string &message): std::invalid_argument(message) {}
};

#endif // CONTAINERS_SIZE_NOT_MATCH_H_
