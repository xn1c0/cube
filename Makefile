##
# cube
#
# @file
# @version 0.1

# Compiler
CXX = g++

# -Wall waring messages
# -std=c++0x some compilers need this to support C++11 (used for nullptr)
CXXFLAGS = -Wall -std=c++0x

main.exe: main.o
	$(CXX) $(MODE) $(CXXFLAGS) main.o -o main.exe

main.o: main.cpp cube.hpp bad_build_size.hpp index_out_of_bounds.hpp containers_size_not_match.hpp
	$(CXX) $(MODE) $(CXXFLAGS) -c main.cpp -o main.o

.PHONY: clean docs run mem
clean:
	rm -rf *.o *.exe
docs:
	doxygen
run:
	make
	./main.exe
mem:
	make
	valgrind --leak-check=full ./main.exe

# end
